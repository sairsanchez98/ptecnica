<?php

use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;




Route::middleware(['auth:sanctum', 'verified'])->get('/', function () {
    return Inertia::render('Dashboard');
})->name('dashboard');


//> AUTH
Route::post('/auth/login' , [App\Http\Controllers\Auth\AuthController::class , 'login'])->name("auth.login");
Route::post('/auth/register' , [App\Http\Controllers\Auth\AuthController::class, 'register'])->name("auth.register");
Route::middleware(["auth:sanctum"])->get('/auth/me' , [App\Http\Controllers\Auth\AuthController::class, 'me'])->name("auth.me");


//>>PRODUCTS //>>
Route::middleware(["auth:sanctum"])->group(function () {
    Route::get('/productos' , [App\Http\Controllers\Product\ProductController::class, 'index'])->name("products.index");
    Route::get('/productos/agregar' , [App\Http\Controllers\Product\ProductController::class, 'create'])->name("products.create");
    Route::post('/productos' , [App\Http\Controllers\Product\ProductController::class, 'store'])->name("products.store");
    Route::get('/productos/{id}' , [App\Http\Controllers\Product\ProductController::class, 'edit'])->name("products.edit");
    Route::put('/productos/', [App\Http\Controllers\Product\ProductController::class, 'update'])->name("products.update");
    Route::delete('/productos/{id}', [App\Http\Controllers\Product\ProductController::class, 'destroy'])->name("products.delete");
    
});



//>>STORES //>>
Route::middleware(["auth:sanctum"])->group(function () {
    Route::get('/tiendas' , [App\Http\Controllers\Store\StoreController::class, 'index'])->name("store.index");
    Route::get('/tiendas/ventas/{id}' , [App\Http\Controllers\Store\StoreController::class, 'showsales'])->name("store.sales");
    Route::get('/all-tiendas' , [App\Http\Controllers\Store\StoreController::class, 'indexall'])->name("stores.all");
    Route::get('/tiendas/agregar' , [App\Http\Controllers\Store\StoreController::class, 'create'])->name("store.create");
    Route::post('/tiendas' , [App\Http\Controllers\Store\StoreController::class, 'store'])->name("store.store");
    Route::get('/tiendas/{id}' , [App\Http\Controllers\Store\StoreController::class, 'edit'])->name("store.edit");
    Route::put('/tiendas/', [App\Http\Controllers\Store\StoreController::class, 'update'])->name("store.update");
    Route::delete('/tiendas/{id}', [App\Http\Controllers\Store\StoreController::class, 'destroy'])->name("store.delete");
});

//>>SALES //>>
Route::middleware(["auth:sanctum"])->group(function () {
    Route::get('/ventas' , [App\Http\Controllers\Sale\SaleController::class, 'index'])->name("sales.index");
    Route::post('/ventas' , [App\Http\Controllers\Sale\SaleController::class, 'store'])->name("sales.store");
    Route::get('/ventas/{id_user}' , [App\Http\Controllers\Sale\SaleController::class, 'show']);
    Route::put('/ventas/', [App\Http\Controllers\Sale\SaleController::class, 'update']);
});

