<?php

namespace App\Http\Controllers\Product;

use App\Http\Controllers\Controller;
use App\Models\Product\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Inertia\Inertia;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $createProduct = $request->createdProduct ? true : false;
        $UpdatedProduct= $request->UpdatedProduct ? true : false;
        $DeleteProduct = $request->DeleteProduct ? true : false;
        $Products = Product::select("*" , DB::raw("concat('$' ,  FORMAT(price, 'N')) as price") , "price as pricenumeric")->latest()->get();
        return Inertia::render("Products/index" , [
            "Products" => $Products,
            "createProduct" => $createProduct,
            "UpdatedProduct" => $UpdatedProduct,
            "DeleteProduct" => $DeleteProduct
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return Inertia::render("Products/create");
    }

   
    public function store(Request $request)
    {

        

        $validator = Validator::make($request->all(),[
            'name' =>'required|string',
            'description' => 'required|string',
            'sku' => 'required|string|unique:products',
            'price' => 'required|numeric'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        $Product = Product::create($request->all());

        
        return  response( $Product,201);

    }


   
    public function edit($id)
    {
        $Product = Product::find($id);

        return Inertia::render("Products/edit", [
            "Product" => $Product
        ]);
    }

    
    public function update(Request $request)
    {
        
        $validator = Validator::make($request->all(),[
            'name' =>'required|string',
            'description' => 'required|string',
            'price' => 'required|numeric'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

    
        $SkuExistente = Product::where("id", "!=" , $request->id )
        ->where("sku" , $request->sku)->get();

        if($SkuExistente->count() > 0){
            return response (["sku" => ["sku ya existe."]] ,  422) ;
        }

        $product = Product::find($request->id);
        $product -> update($request->all());
        return response ( $product , 201);


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Product::find($id);
        $product->delete();
        return response ( "deleted!" , 201);
    }
}
