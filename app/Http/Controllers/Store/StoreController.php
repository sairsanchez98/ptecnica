<?php

namespace App\Http\Controllers\Store;

use App\Http\Controllers\Controller;
use App\Models\Sale\Sale;
use App\Models\Store\Store;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Inertia\Inertia;

class StoreController extends Controller
{
    
    public function index(Request $request)
    {
        $createStore = $request->createdStore ? true : false;
        $UpdatedStore= $request->UpdatedStore ? true : false;
        $DeleteStore = $request->DeleteStore ? true : false;

        $Stores = Store::latest()->get();

        foreach ($Stores as $store) {
            $totalNumVentas = Sale::select(DB::raw("COUNT(id) as totalNumVentas") )
            ->where("sales.store_id" , $store->id)
            ->first();
            
            $store["totalNumVentas"] = $totalNumVentas["totalNumVentas"];
        }

        return Inertia::render("Stores/index" , 
        [
            "Stores" => $Stores,
            "createStore" => $createStore,
            "UpdatedStore" => $UpdatedStore,
            "DeleteStore" => $DeleteStore
        ]
        );
    }

    public function indexall (){
        $Stores = Store::all();
        return response( $Stores , 201);
    }

    public function create()
    {
        return Inertia::render("Stores/create");
    }

  
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'name' =>'required|string',
            "latitude" => 'required|string',
            "longitude" => 'required|string',
            "opening"=> 'required|string'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        $Store = Store::create($request->all());

        
        return  response( $Store,201);
    }

  
    
    public function edit($id)
    {
        $Store = Store::find($id);

        return Inertia::render("Stores/edit", [
            "Store" => $Store
        ]);
    }

 
    public function update(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'name' =>'required|string',
            "latitude" => 'required|string',
            "longitude" => 'required|string',
            "opening"=> 'required|string'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

    
      
        $Store = Store::find($request->id);
        $Store -> update($request->all());
        return response ( $Store , 201);


    }

  
    public function destroy($id)
    {
        $Store = Store::find($id);
        $Store->delete();
        return response ( "deleted!" , 201);
    }


    public function showsales($id){
        $Sales = Sale::where("store_id" , $id)
        ->select("sales.*" , "stores.name as name_store", "products.name as name_product", "stores.*" , "products.*", 
            DB::raw("concat('$' , FORMAT ( sales.unit ,' N')) as unitformat , concat('$' , FORMAT ( sales.total ,' N')) as totalformat ") 
        )
        ->join("stores" , "stores.id" , "=" , "sales.store_id")
        ->join("products" , "products.id" , "=" , "sales.product_id")
        ->latest("sales.created_at")->get();

        $grandTotal = Sale::select(
            DB::raw("concat( '$' , FORMAT (SUM(total) , 'N')) as grandTotal")
        )->where("store_id"  ,  $id)->first();


        return Inertia::render("Stores/showsales" , [
            "Sales" => $Sales,
            "grandTotal" => $grandTotal->grandTotal
        ]);
    }
}
