<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title inertia>{{ 'PTECNICA' }}</title>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap">

    <!-- Styles -->
    <!-- <link rel="stylesheet" href="{{ mix('css/app.css') }}"> -->

    <!-- include common vendor stylesheets & fontawesome -->
    <link rel="stylesheet" type="text/css" href="{{asset('template/node_modules/bootstrap/dist/css/bootstrap.css')}}">

    <link rel="stylesheet" type="text/css" href="{{asset('template/node_modules/@fortawesome/fontawesome-free/css/fontawesome.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('template/node_modules/@fortawesome/fontawesome-free/css/regular.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('template/node_modules/@fortawesome/fontawesome-free/css/brands.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('template/node_modules/@fortawesome/fontawesome-free/css/solid.css')}}">

    <!-- include fonts -->
    <link rel="stylesheet" type="text/css" href="{{asset('template/dist/css/ace-font.css')}}">
    <!-- ace.css -->
    <link rel="stylesheet" type="text/css" href="{{asset('template/dist/css/ace.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('template/dist/css/bootstrap-select.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('template/node_modules/select2/dist/css/select2.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('template/node_modules/chosen-js/chosen.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('template/node_modules/nouislider/distribute/nouisldier.min.css')}}">

    <link rel="stylesheet" type="text/css" href="{{asset('template/node_modules/ion-rangeslider/css/ion.rangeSlider.min.css')}}">

    <link rel="stylesheet" type="text/css" href="{{asset('template/node_modules/smartwizard/dist/css/smart_wizard.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('template/node_modules/smartwizard/dist/css/smart_wizard_theme_circles.min.css')}}">

    <link rel="stylesheet" type="text/css" href="{{asset('template/node_modules/tiny-date-picker/tiny-date-picker.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('template/node_modules/tiny-date-picker/date-range-picker.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('template/node_modules/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('template/node_modules/bootstrap-table/dist/bootstrap-table.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('template/node_modules/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.css')}}">

    <link rel="stylesheet" type="text/css" href="{{asset('template/node_modules/datatables.net-bs4/css/dataTables.bootstrap4.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('template/node_modules/datatables.net-buttons-bs4/css/buttons.bootstrap4.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('template/node_modules/dropzone/dist/dropzone.css')}}">

    <link rel="stylesheet" type="text/css" href="{{asset('template/node_modules/summernote/dist/summernote-lite.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('template/node_modules/bootstrap-markdown/css/bootstrap-markdown.min.css')}}">

    <link rel="stylesheet" type="text/css" href="{{asset('template/views/pages/form-wysiwyg/@page-style.css')}}">

    <link rel="stylesheet" type="text/css" href="{{asset('template/views/pages/cards/@page-style.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('template/views/pages/dashboard/@page-style.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('template/views/pages/dashboard-2/@page-style.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('template/views/pages/dashboard-4/@page-style.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('template/views/pages/table-datatables/@page-style.css')}}">

  
    
    <link rel="stylesheet" type="text/css" href="{{asset('template//node_modules/jam-icons/css/jam.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('template//node_modules/eva-icons/style/eva-icons.css')}}">




    
    <link rel="stylesheet" type="text/css" href="{{asset('template/dist/css/ace-themes.css')}}">
    @routes
    <script src="{{ asset('js/app.js') }}" defer></script>
</head>

<body class="font-sans antialiased">
    @inertia

    @env ('local')
    <script src="http://localhost:3000/browser-sync/browser-sync-client.js"></script>
    @endenv
</body>


<script src="{{asset('template/node_modules/jquery/dist/jquery.js')}}"></script>
<script src="{{asset('template/node_modules/popper.js/dist/umd/popper.js')}}"></script>
<script src="{{asset('template/node_modules/bootstrap/dist/js/bootstrap.js')}}"></script>

<!-- include vendor scripts used in "Alerts" page. see "/views//pages/partials/alerts/@vendor-scripts.hbs" -->
<script src="{{asset('template/node_modules/sweetalert2/dist/sweetalert2.all.js')}}"></script>
<script src="{{asset('template/node_modules/interactjs/dist/interact.js')}}"></script>

<!-- include vendor scripts used in "DataTables" page. see "/views//pages/partials/table-datatables/@vendor-scripts.hbs" -->
<script src="{{asset('template/node_modules/datatables/media/js/jquery.dataTables.js')}}"></script>
<script src="{{asset('template/node_modules/datatables.net-bs4/js/dataTables.bootstrap4.js')}}"></script>
<script src="{{asset('template/node_modules/datatables.net-colreorder/js/dataTables.colReorder.js')}}"></script>
<script src="{{asset('template/node_modules/datatables.net-select/js/dataTables.select.js')}}"></script>
<script src="{{asset('template/node_modules/datatables.net-buttons/js/dataTables.buttons.js')}}"></script>
<script src="{{asset('template/node_modules/datatables.net-buttons-bs4/js/buttons.bootstrap4.js')}}"></script>
<script src="{{asset('template/node_modules/datatables.net-buttons/js/buttons.html5.js')}}"></script>
<script src="{{asset('template/node_modules/datatables.net-buttons/js/buttons.print.js')}}"></script>
<script src="{{asset('template/node_modules/datatables.net-buttons/js/buttons.colVis.js')}}"></script>


<script src="{{asset('template/node_modules/datatables.net-responsive/js/dataTables.responsive.js')}}"></script>

<!-- include vendor scripts used in "Dashboard" page. see "/views//pages/partials/dashboard/@vendor-scripts.hbs" -->
<script src="{{asset('template/node_modules/chart.js/dist/Chart.js')}}"></script>
<script src="{{asset('template/node_modules/sortablejs/dist/sortable.umd.js')}}"></script>
<!-- include ace.js -->
<script src="{{asset('template/dist/js/ace.js')}}"></script>
<!-- demo.js is only for Ace's demo and you shouldn't use it -->
<script src="{{asset('template/app/browser/demo.js')}}"></script>

<!-- "DataTables" page script to enable its demo functionality -->
<script src="{{asset('template/views/pages/table-datatables/@page-script.js')}}"></script>

<!-- "Dashboard" page script to enable its demo functionality -->


<script src="{{asset('template/views/pages/dashboard/@page-script.js')}}"></script>
<script src="{{asset('template/views/pages/dashboard-3/@page-script.js')}}"></script>
<!-- "Alerts" page script to enable its demo functionality -->
<script src="{{asset('template/views/pages/asides/@page-script.js')}}"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>


<!-- "Login" page script to enable its demo functionality -->
<script src="{{asset('template/views/pages/page-login/@page-script.js')}}"></script>




</html>