<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class CreateUserDefault extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            "name" => "SAIR SANCHEZ VALDERRAMA",
            "email" => "sairsanchez29@gmail.com",
            "password" => bcrypt("pt981129(*)")
        ]);
    }
}
